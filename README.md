# laser-grader

Attempt to build a DIY laser grader, including 25cm high receivers for a rotary construction laser, and automatic control of 2 hydraulic actuators for leveling a shovel. The plan is to build an attachment for a tractor, loader, Unimog etc. basically any vehicle which can push or pull an attachment and has auxilary hydraulics.

## Electronics
- Discussion on building a rotary-laser receiver: https://forum.allaboutcircuits.com/threads/rotary-laser-level-receiver-circuit.155616/
- idea from https://forum.arduino.cc/index.php?topic=144045.0 to use a shift register with a latch (i.e. 74HC597: https://assets.nexperia.com/documents/data-sheet/74HC_HCT597.pdf)
- example circuit using 5 upper and 5 lower diodes: https://circuit-diagramz.com/laser-level-detector-schematic-circuit-diagram/
- more discussion (not very helpful): https://www.electro-tech-online.com/threads/rotary-laser-level-detector-receiver-please.125296/
- Photodiode: Osram BP104 looks ok, Osram BPW34 has better sensitivity at 630nm but worse wide angle sensitivity, both 4mm wide, Osram SFH2701 only 1.6mm wide (https://docs.rs-online.com/2974/0900766b80d00bd0.pdf) but more expensive and less available)
- possible problem: might need to detect the laser's small increase of light intensivity over the ambient daylight level, on all photodiodes simultaneously, so simple analog triggering the flipflops might not be enough
- paper on such a project, unfortunately without schematic https://www.mdpi.com/2079-9292/9/3/536/pdf
- interesting info on OPA capacitor calulation: https://e2e.ti.com/support/amplifiers/f/14/t/902973
- LM324 a good solution? https://electronics.stackexchange.com/questions/192224/photodiode-amplifier-circuit

concept: >=25cm long pcb with >=64 high speed photodiodes (1µs pulse duration!) which feed a latching shift register. When any of the photodiodes trigger (=sense a laser pulse), the shift register inputs must be latched and the microprocessor notified by a hardware interrupt, then it serially reads the shift register and then resets the latches. Then the timing (since the previous pulse) is compared to the expected pulse frequency (~10Hz at 600 laser rpm) and if it is within the limits, the height is calculated from the triggered photodiode position and a) communicated to any visual or audible display and b) used to correct the hydraulic actuators.


## Hydraulics/mechanics
- 12V Hydraulic valve block for controlling 2 bidirectional actuators: https://www.ebay.de/itm/303643722282 (320€)
- 2 Actuators (200mm) and hydraulic hoses and connectors: ~200€ on ebay
